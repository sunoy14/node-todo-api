var env = process.env.NODE_ENV || 'development';

if(env === 'development'){
	process.env.PORT = 3000;
	process.env.MONGODB_URI ='mongodb://localhost:27017/rom-link_test';
} 
else if(env === 'test'){
	process.env.PORT = 3000;
	process.env.MONGODB_URI = 'mongodb://localhost:27017/rom-link_testTest';
}

if(process.env.MONGODB_URI === undefined){
	process.env.MONGODB_URI = 'mongodb://user:password@ds213688.mlab.com:13688/rom-link_test';
}

