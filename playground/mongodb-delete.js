//const MongoClient = require('mongodb').MongoClient;
const {MongoClient, ObjectID} = require('mongodb');

//var obj = new ObjectID();
//console.log(obj);
//console.log(obj.getTimestamp());

MongoClient.connect('mongodb://localhost:27017/TodoApp', (err, db) => {
	if(err){
		return console.log('Unable to connect to MongoDB server');
	}
	console.log('Connected to MongoDB server');

	//db.collection('Todos').deleteMany({text: 'Eat lunch'}).then((result) => {
	//	console.log(result);
	//});

	//db.collection('Todos').deleteOne({text: 'Eat lunch'}).then((result) => {
	//	console.log(result);
	//});

//	db.collection('Todos').findOneAndDelete({completed: false}).then((result) => {
//		console.log(result);
//	});
	
	//db.collection('Users').deleteMany({name: 'Millo Lailang'});

	db.collection('Users').findOneAndDelete({
		_id: new ObjectID('5a6eb625a5eb0020d4ffc329')
	}).then((result) => {
		console.log(JSON.stringify(result, undefined, 2));
	});
	//db.close();
});
