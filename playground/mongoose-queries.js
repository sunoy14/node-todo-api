const {ObjectID} = require('mongodb');

const {mongoose} = require('./../server/db/mongoose');
const{Todo} = require('./../server/models/todo.js');
const{User} = require('./../server/models/user');

var id = '5a740c2edcfcd8353360c104';
var userID = '5a725d4a581c180882be8ff2';

if(!ObjectID.isValid(id)){
	console.log('ID not valid');
}

//Todo.find({
//	_id: id
//}).then((todos) => {
//	if(todos.length === 0){
//		return console.log('Id not found');
//	}
//	console.log('Todos', todos);
//});
//
//Todo.findOne({
//	_id: id
//}).then((todo) => {
//	if(!todo){
//		return console.log('Id not found');
//	}
//	console.log('Todo', todo);
//});

Todo.findById(id).then((todo) => {
	if(!todo){
		return console.log('Id not found');
	}
	console.log('Todo by id', todo);
}).catch((e) => console.log(e));

User.findById(userID).then((user) => {
	if(!user){
		console.log('Unable to find user');
		return;
	}
	console.log('User by id', JSON.stringify(user, undefined, 2));
}, (e) => {
	console.log(e);
});
