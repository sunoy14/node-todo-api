const {ObjectID} = require('mongodb');

const {mongoose} = require('./../server/db/mongoose');
const{Todo} = require('./../server/models/todo.js');
const{User} = require('./../server/models/user');

//Todo.remove({}).then((result) => {
//	console.log(result);
//});

Todo.findOneAndRemove({_id: '5a779008911025bd0c70bc3c'}).then((todo) => {
	console.log(todo);
});

//Todo.findByIdAndRemove('5a778e3a3cd31faf34e00a52').then((todo) => {
//	console.log(todo);
//});
