//const MongoClient = require('mongodb').MongoClient;
const {MongoClient, ObjectID} = require('mongodb');

//var obj = new ObjectID();
//console.log(obj);
//console.log(obj.getTimestamp());

MongoClient.connect('mongodb://localhost:27017/TodoApp', (err, db) => {
	if(err){
		return console.log('Unable to connect to MongoDB server');
	}
	console.log('Connected to MongoDB server');

	//db.db('TodoApp').collection('Todos').insertOne({   // for npm mongodb@3.0 or greater
//	db.collection('Todos').insertOne({	// for npm mongodb@2.22.3
//		text: 'Something to do',
//		completed: false
//	}, (err, result) => {
//		if(err){
//			return console.log('Unable to insert todo', err);
//		}
//
//		console.log(JSON.stringify(result.ops, undefined, 2));
//	});
	
	db.collection('Users').insertOne({
		name: 'Millo',
		age: 32,
		location: 'Itanagar'
	}, (err, result) => {
		if(err){
			return console.log('Unable to insert user', err);
		}
		console.log(JSON.stringify(result.ops, undefined, 2));
		console.log(result.ops[0]._id.getTimestamp());
	});

	db.close();
});
