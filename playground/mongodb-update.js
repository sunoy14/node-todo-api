//const MongoClient = require('mongodb').MongoClient;
const {MongoClient, ObjectID} = require('mongodb');

//var obj = new ObjectID();
//console.log(obj);
//console.log(obj.getTimestamp());

MongoClient.connect('mongodb://localhost:27017/TodoApp', (err, db) => {
	if(err){
		return console.log('Unable to connect to MongoDB server');
	}
	console.log('Connected to MongoDB server');

	//db.collection('Todos').findOneAndUpdate({
	//	text: 'Eat lunch'
	//}, {
	//	$set: {
	//		text: 'Eat lunch 2'
	//	}
	//}, {
	//	returnOriginal: false

	//}).then((result) => {
	//	console.log(result);
	//});
	
	db.collection('Users').findOneAndUpdate({
		name: 'Millo Lailang the Great'
	}, {
		$set: {
				name: 'Millo Lailang the Greatest'
		},
		$inc: {
			age: +1
		}
	}, {
		returnOriginal: false
	}).then((result) => {
		console.log(result);
	});

	//db.close();
});
